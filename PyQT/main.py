#!/usr/bin/env python3
from PyQt5.QtWidgets import QApplication
import sys
from ui import Ui_Form

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Ui_Form()
    ex.show()
    sys.exit(app.exec_())
