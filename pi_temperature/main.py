#!/usr/bin/env python3
from subprocess import Popen, PIPE
from datetime import datetime


now = datetime.now()
logfile = '/home/pi/temperature_{0.day}_{0.month}_{0.year}.log'.format(now)


if __name__ == '__main__':
    with open(logfile, 'ab') as f:
        while True:
            process = Popen('/opt/vc/bin/vcgencmd measure_temp',
                            shell=True, stdout=PIPE)
            for line in process.stdout:
                f.write(line)
