#! /usr/bin/python3
from packages import backup, choose_between_two

print("This program will backup packages from your system to directory specified by you.")

choose_between_two(input_info_string = "Do you really want to do this (it may take a lot of time)?",
first_func = backup,
second_func = exit,
first_value = 'yes',
second_value = 'no')
