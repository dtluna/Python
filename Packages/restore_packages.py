#! /usr/bin/python3
from packages import restore, choose_between_two

print("This program will restore your packages.")

choose_between_two(input_info_string = "Do you really want to do this (it may take a lot of time)?",
first_func = restore,
second_func = exit,
first_value = 'yes',
second_value = 'no')