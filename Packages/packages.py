from subprocess import call
import  os, time

def choose_between_two (input_info_string, first_value, second_value, first_func, second_func):
	while True:
		answer = input(input_info_string + '(' + first_value + ', ' + second_value + '): ')
		#print(first_value)
		#print(second_value)
		if answer == first_value:
			first_func()
		elif answer == second_value:
			second_func()
		else:
			print('Answer only with ' + first_value + ' or '+ second_value + '!\n')

def install_dpkg_repack():
	"""Function to install Debian package 'dpkg-repack'."""
	call('sudo apt-get install dpkg-repack')

def check_for_dpkg_repack():
	if os.path.exists('/usr/bin/dpkg-repack') == False:
		print("'dpkg-repack' is not installed in your system.") 
		choose_between_two("Do you want to install 'dpkg-repack'?", 'yes', 'no', install_dpkg_repack, exit) 

def form_package_list_backup():
	if os.path.exists('temp.txt'):
		os.remove('temp.txt')
	call('dpkg --get-selections > temp.txt', shell=True)
	packages_file = open('temp.txt')
	
	package_list = packages_file.readlines()
	packages_file.close()
	call('rm temp.txt', shell=True)
	
	for package in package_list:
		pos = package.find('deinstall')
		if pos == -1:
			package_list[package_list.index(package)] = package[0:pos]+'\n'
		
	
	print('Your current package selection is printed into %s file' % ('package-selection-%s.txt' % time.asctime().replace(' ', '-')))
	packages_file = open('package-selection-%s.txt' % time.asctime().replace(' ', '-'), 'w')
	packages_file.writelines(package_list)
	
	return package_list

def backup():
	directory = input("Specify the directory where to backup your packages:\n")

	if os.path.isdir (directory):
		os.chdir(directory)

		package_list = form_package_list_backup()

		for package in package_list:
			call_string = 'sudo dpkg-repack %s' % package
			print (package_list.index(package)+1, call_string)
			call(call_string, shell=True)
			print("\n")

		exit()
		
	else:
		print('%s: no such directory. Aborting.' % directory_name)
		exit()

def form_package_list_restore(packages_file_name):
	packages_file = open(packages_file_name)
	package_list = packages_file.readlines()
	packages_file.close() 
		
	for package in package_list:
		package_list[package_list.index(package)] = package.rstrip()

	return package_list

def download_from_repository():
	packages_file_name = input('Specify the file with packagenames:')
	if os.path.exists(packages_file_name):
		package_list = form_package_list_restore(packages_file_name)
		answer = input("Do you want for program to answer 'yes' to all the questions of apt(this can be VERY dangerous)?(yes,no)")
		if answer == 'yes':
			call_string = 'sudo apt-get install -y ' 
		else:
			call_string = 'sudo apt-get install ' 
		for package in package_list:
			print('%d. %s' % (package_list.index(package)+1, call_string + package))
			call(call_string + package, shell=True)
		exit()
	else:
		print('No such file or directory: %s' % packages_file_name)
		exit()

def restore_backed_up():
	directory = input("Specify the directory where your packages backed up are:\n")
	if os.path.isdir (os.path.realpath(directory)):
		os.chdir(directory)
		call('ls -1 %s > temp.txt' % os.curdir, shell=True)
		package_list = form_package_list_restore('temp.txt')
		os.remove('temp.txt')
		for package in package_list:
			if package[-4:] == '.deb':
				call_string = 'sudo dpkg -i %s' % package
				call(call_string, shell=True)
				#print(package)
				#package_list.remove(package)
		exit()				
			
	else:
		print('%s: no such directory. Aborting.' % directory_name)
		exit()
	
def restore():
	choose_between_two(input_info_string = "Do you want to read packagenames from file and download them from repository(rep) or restore backed up packages from directory(dir)?", 
	first_value = 'rep',
	second_value = 'dir',
	first_func = download_from_repository,
	second_func= restore_backed_up)
