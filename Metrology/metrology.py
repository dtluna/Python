results = [12.7416, 12.8033, 13.3574, 12.7983, 12.5663, 12.7133, 12.9213,
12.7064, 12.7432, 12.7428, 13.5213, 12.8330, 12.3214, 13.3946, 13.4483]
n = 15

res_sum = sum(results)
x_average = res_sum/n
print("Среднее значение: %f мс" % x_average)

from math import sqrt

sum_sq_dev = 0.0 #сумма квадратичных отклонений
for x in results:
	sum_sq_dev = sum_sq_dev + pow(x-x_average,2)

print("Сумма квадратичных отклонений: %f" % sum_sq_dev)

sd = sqrt( (1/(n-1))*sum_sq_dev)#sd(standard deviation) — стандартное отклонение
print("СКО = %f мс" % sd)

for x in results:
	if abs(x-x_average) > 3*sd:
		del x
		print ("Удален результат %f из-за грубой погрешности" % x)

se = sd/sqrt(n)#se(standard error) — СКП
print("СКП = %f мс" % se)

P=0.95
t=2.145
delta = t*se
print ("Результат = %f+-%f мс" % (x_average, delta))
		