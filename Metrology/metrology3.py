from math import sqrt
#E= U**2*t/R
U=280
R=1450
t=15
sigma_U=15
sigma_R=80
sigma_t=0.8
delta_syst_U=0.8
delta_syst_R=0.3
delta_syst_t=0.45
P=0.95
K=1.1

dEdU = 2*U*t/R
dEdR = -U**2*t/R**2
dEdt = U**2/R

delta_syst_E_1 = K*sqrt((delta_syst_U*dEdU)**2 + (delta_syst_R*dEdR)**2 + (delta_syst_t*dEdt)**2)
print("Δс' = %f Дж" % delta_syst_E_1)

delta_syst_E_2 = dEdU*delta_syst_U + dEdR*delta_syst_R + dEdt*delta_syst_t
print("Δс'' = %f Дж" % delta_syst_E_2)

delta_syst_E = min(delta_syst_E_1,delta_syst_E_2)
print("Границы суммарной систематической погрешности: ΔcE = %f Дж" % delta_syst_E)

sigma_E = sqrt((sigma_U*dEdU)**2 +  (sigma_R*dEdR)**2 + (sigma_t*dEdt)**2)
print("СКП результата косвенных измерений: σE = %f Дж" % sigma_E)

mu_E = sqrt(delta_syst_E/sigma_E)
print ('µE = %f' % mu_E)

if mu_E<0.5:
	delta_E = 2*sigma_E #коээфициент Стьюдента равен двум при P=0.95 для однократных измерений
elif mu_E > 8:
	delta_E = delta_syst_E
else:
	delta_E = sqrt(delta_syst_E**2 + (2*sigma_E)**2)#для косвенных измерений

print("ΔE = %f Дж" % delta_E)

E = (t*U**2)/R

print ("Результат измерений: E = %3f +- %3f Дж" % (E,delta_E))
