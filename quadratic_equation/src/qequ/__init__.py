from math import sqrt
from os.path import abspath, join, dirname


def discriminant(a, b, c):
    return (b**2 - 4*a*c)


def get_roots(a, b, c, D):
    if a == 0:
        return -c/b
    if D < 0:
        return None
    if D == 0:
        return -b/(2*a)
    elif D > 0:
        first_root = (-b + sqrt(D))/2*a
        second_root = (-b - sqrt(D))/2*a
        return (first_root, second_root)


equation = "{:g}x^2 {:+g}x {:+g} = 0"

app_dir = abspath(dirname(__file__))
locale_dir = join(app_dir, 'locale')
