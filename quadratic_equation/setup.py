#!/usr/bin/env python3
import codecs
from setuptools import setup
try:
    codecs.lookup('mbcs')
except LookupError:
    ascii = codecs.lookup('ascii')

    def func(name, enc=ascii):
        return {True: enc}.get(name == 'mbcs')
    codecs.register(func)


def read(file):
    with open(file) as f:
        return f.read()

setup(name='qequ',
      version='1.0.2',
      description='Command-line quadratic equation solver',
      long_description=read('README'),
      author='Konstantin Nikitin',
      author_email='dreamtheaterluna@gmail.com',
      maintainer='Konstantin Nikitin',
      maintainer_email='dreamtheaterluna@gmail.com',
      license='GPLv3',
      keywords=['quadratic', 'equation', 'command-line'],
      classifiers=['Development Status :: 5 - Production/Stable',
                   'Environment :: Console',
                   'Intended Audience :: Education',
                   'Intended Audience :: End Users/Desktop',
                   'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
                   'Natural Language :: English',
                   'Natural Language :: Russian',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 3',
                   'Programming Language :: Python :: Implementation',
                   'Topic :: Education'],
      url='https://github.com/DTLuna/Python/tree/master/quadratic_equation',
      platforms=['any'],
      scripts=['scripts/qequ'],
      packages=['qequ'],
      package_dir={'qequ': 'src/qequ'},
      include_package_data=True
      )
