# Tic-tac-toe game
# by DT


def show_board(board):
    print(board[0], '|', board[1], '|', board[2])
    print('---------')
    print(board[3], '|', board[4], '|', board[5])
    print('---------')
    print(board[6], '|', board[7], '|', board[8])


def check_victory(board):
    return ((board[0] == board[1] == board[2]) or
            (board[3] == board[4] == board[5]) or
            (board[6] == board[7] == board[8]) or
            (board[0] == board[3] == board[5]) or
            (board[1] == board[4] == board[7]) or
            (board[2] == board[5] == board[8]) or
            (board[0] == board[4] == board[8]) or
            (board[2] == board[4] == board[6]))


def check_draw(board):
    cells_left = len(board)
    for i in board:
        if i == 'x' or i == 'o':
            cells_left = cells_left - 1
    if cells_left < 1:
        return True
    else:
        return False


def player_turn(board, player_symbol):
    print("")
    while True:
        print(player_symbol, "turn!")
        show_board(board)
        try:
            spot = int(input("Select a spot: "))
            if (spot > 9) or (spot < 1):
                raise ValueError
        except ValueError:
            print("Invalid spot number. Try again.\n")
            continue
        except (EOFError, KeyboardInterrupt):
            exit()
        if board[spot-1] != 'x' and board[spot-1] != 'o':
            board[spot-1] = player_symbol

            if check_victory(board):
                print(player_symbol, "wins!")
                show_board(board)
                quit()

            if check_draw(board):
                print("\nDraw!")
                show_board(board)
                quit()
            break
        else:
            print("This spot is taken!")


def game():

    board = [1, 2, 3,
             4, 5, 6,
             7, 8, 9]

    print("Sup bros!")
    while True:
        try:
            answer = input("Who's gonna be first? (o,x):\n")
        except (EOFError, KeyboardInterrupt):
            exit()

        if answer == 'o':
            first = 'o'
            second = 'x'
            break
        elif answer == 'x':
            first = 'x'
            second = 'o'
            break
        else:
            print("You have only two options. No less. No more.\n")

    print("If you wish to force-quit the game, press Ctrl-D.")
    while True:
        player_turn(board, first)
        player_turn(board, second)
