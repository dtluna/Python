#!/usr/bin/env python3

from peewee import SqliteDatabase, Model, CharField

db = SqliteDatabase('people.db')

class Person(Model):
    name = CharField()

    class Meta:
        database = db # This model uses the "people.db" database.

if __name__ == '__main__':
    db.connect()
