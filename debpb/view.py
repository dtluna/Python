import const
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


ui_file = 'ui.glade'
builder = Gtk.Builder()
builder.add_from_file(ui_file)



def add_row(self):
    def add_part_row(side):
        self.attach_next_to(
            child=Gtk.Label('cell'),
            sibling=self.get_child_at(left=side, top=self.last_row),
            side=Gtk.PositionType.BOTTOM,
            width=1,
            height=1
        )
    add_part_row(const.LEFT)
    add_part_row(const.RIGHT)
    self.last_row += 1
    self.show_all()


setattr(Gtk.Grid, 'add_row', add_row)
setattr(Gtk.Grid, 'last_row', 0)


class View(object):
    _window = builder.get_object('window')
    _backup_grid = builder.get_object('_backup_grid')
    _restore_grid = builder.get_object('_restore_grid')

    def __init__(self, controller):
        self.window.connect('destroy', Gtk.main_quit)
        self.window.show_all()

        builder.connect_signals(controller)

    def update(self):
        self.window.show_all()

    @property
    def window(self):
        return self._window

    @property
    def backup_grid(self):
        return self._backup_grid

    @property
    def restore_grid(self):
        return self._restore_grid
