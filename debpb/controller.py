import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class Controller(object):
    _view = None
    _instanced = False

    def __init__(self):
        object.__init__(self)
        self._instanced = True

    @property
    def view(self):
        return self._view

    @view.setter
    def view(self, view):
        if self.view is not None:
            raise RuntimeError("You cannot set view in controller twice!")
        else:
            self._view = view

    def backup(widget=None, data=None):
        print('Backup pressed')

    def restore(widget=None, data=None):
        print('Restore pressed')

    def add_row(self, grid):
        LEFT = 0
        RIGHT = 1
        def add_part_row(contents):
            grid = Gtk.Grid()
            grid.attach_next_to(
                child=Gtk.Label(str(contents)),
                sibling=None,
                side=Gtk.PositionType.BOTTOM,
                width=1,
                height=1
            )

        # def add_part_row(self, contents, side):
        #     self.info_grid.attach_next_to(Gtk.Label(str(contents)),
        #         self.info_grid.get_child_at(side, self.info_grid.last_row),
        #         Gtk.PositionType.BOTTOM, 1, 1)
        #
        # def add_row(self, left_column, right_column):
        #     self.add_part_row(left_column, left)
        #     self.add_part_row(right_column, right)
        #     self.info_grid.last_row += 1