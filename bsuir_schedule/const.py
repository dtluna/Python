from os import curdir, sep
from os.path import abspath

subgroup_values = ('0', '1', '2')
config_file = abspath(curdir + sep + 'schedule.cfg')

all_groups_xml_url = 'http://www.bsuir.by/schedule/rest/studentGroup'
all_groups_xml = 'all_groups.xml'
group_url = 'http://www.bsuir.by/schedule/rest/schedule/%s'
group_schedule_xml = '%s.xml'

weekdays_in_russian = ('Понедельник', 'Вторник', 'Среда',
                       'Четверг', 'Пятница', 'Суббота', 'Воскресенье')
