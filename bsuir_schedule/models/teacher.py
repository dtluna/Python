class Teacher(object):
    name = None
    surname = None
    patronymic = None
    academic_department = None
    id = None

    def __init__(self, name, surname, patronymic, academic_department, id):
        self.name = name
        self.surname = surname
        self.patronymic = patronymic
        self.academic_department = academic_department
        self.id = id
