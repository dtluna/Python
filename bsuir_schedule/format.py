#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from xmlformatter import Formatter
from glob import glob

form = Formatter(encoding_input='UTF-8', encoding_output='UTF-8')
xmls = glob('*.xml')


if __name__ == '__main__':
    with open('350501f.xml', 'w') as f:
        f.write(form.format_file('350501.xml').decode("utf-8", "strict"))
