from xml.etree.ElementTree import parse
from urllib.request import urlopen
from datetime import date
from config import config
from const import all_groups_xml_url, all_groups_xml,   \
                  group_url, group_schedule_xml,        \
                  weekdays_in_russian
from lesson import Lesson
from day_schedule import DaySchedule
from teacher import Teacher


def get_calendar_week(year, month, day):
    """1 — поле с колендарной неделей"""
    return date(year, month, day).isocalendar()[1]


today = date.today()
today_week = get_calendar_week(today.year, today.month, today.day)


first_of_september_week = get_calendar_week(today.year, 9, 1)


current_date = today


def load_schedule(group_name):
    return parse(group_schedule_xml % (group_name))


def download_all_groups_xml():
    u = urlopen(all_groups_xml_url)
    xml_data = u.read()
    with open(all_groups_xml, 'wb') as f:
        f.write(xml_data)


def download_group_schedule(group_name):
    group_id = get_id_by_group_name(group_name)
    u = urlopen(group_url % (group_id))
    xml_data = u.read()
    with open(group_schedule_xml % (group_name), 'wb') as f:
        f.write(xml_data)


def get_id_by_group_name(group_name):
    with parse(all_groups_xml) as doc:
        for group in doc.findall('studentGroup'):
            if group.findtext('name') == group_name:
                return group.findtext('id')
    raise LookupError("No such group name")


def get_day_schedule(week, weekday, group_name=config['group_name'],
                     subgroup=config['subgroup']):
    """Дёргает из DOM нужные поля,
    и воозвращает список(list) пар на день(класс).
    weekday должен быть на русском
    week:int"""
    day_schedule = DaySchedule(current_date, week, subgroup)
    doc = load_schedule(group_name)
    for schedule_model in doc.findall('scheduleModel'):
        if schedule_model.findtext('weekDay') == weekday:
            fill_day_schedule(schedule_model, day_schedule, week, subgroup)
    return day_schedule


def fill_day_schedule(schedule_model, day_schedule, week, subgroup):
    for schedule in schedule_model.findall('schedule'):
        if (schedule.findtext('numSubgroup') == str(subgroup) or
           schedule.findtext('numSubgroup') == '0'):
            for weekNumber in schedule.iter('weekNumber'):
                if weekNumber.text == str(week):
                    lesson = get_lesson_object_from_schedule(schedule)
                    day_schedule.add_lesson(lesson)


def get_teacher_object_from_employee(employee_doc):
    teacher_name = employee_doc.find('firstName').text
    teacher_surname = employee_doc.find('lastName').text
    teacher_patronymic = employee_doc.find('middleName').text
    teacher_academic_department = employee_doc.find('academicDepartment').text
    teacher_id = employee_doc.find('id').text

    teacher = Teacher(teacher_name,
                      teacher_surname,
                      teacher_patronymic,
                      teacher_academic_department,
                      teacher_id)
    return teacher


def get_lesson_object_from_schedule(schedule):
    employee_doc = schedule.find('employee')

    teacher = get_teacher_object_from_employee(employee_doc)

    subject = schedule.find('subject').text
    time = schedule.find('lessonTime').text
    type = schedule.find('lessonType').text
    auditory = schedule.find('auditory').text
    subgroup = schedule.find('numSubgroup').text

    lesson = Lesson(subject,
                    type,
                    auditory,
                    subgroup,
                    time,
                    teacher)
    return lesson


def get_weekday_in_russian(weekday=today.weekday()):
    """weekday: int"""
    return weekdays_in_russian[weekday]


def get_week_number(day, month):
    """Возращает int номер недели по дню и месяцу"""
    week = get_calendar_week(today.year, month, day)
    return (today_week - first_of_september_week) % 4 + 1

if __name__ == '__main__':
    # How to use object model of schedule:
    schedule = get_day_schedule(1, 'Среда', 350501, subgroup=2)
    for lesson in schedule.lessons:
        print(lesson.subject + ': ' + lesson.type)
        print(lesson.auditory)
        print('%s %s %s' % (lesson.teacher.surname,
                            lesson.teacher.name,
                            lesson.teacher.patronymic))
        print('------------ \n')
