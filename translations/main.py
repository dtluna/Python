#!/usr/bin/env python3
# To generate trnaslation templates use xgettext or something similar.
# Then translate the genetated .po file with something like poedit.

import gettext
from os import curdir, sep
from os.path import abspath

app_dir = abspath(curdir)
locale_dir = app_dir + sep + 'locale'

gettext.install('messages', locale_dir)

if __name__ == '__main__':
    print(_('Hello world!'))
