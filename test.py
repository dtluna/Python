#!/usr/bin/python3
import math
import pylab
from matplotlib import mlab

def func (x):
    if x == 0:
        return 1.0
    return math.sin(x) / x

xmin = -40.0
xmax = 40.0

dx = 0.01

xlist = mlab.frange(xmin, xmax, dx)

ylist1 = [func(x) for x in xlist]
ylist2 = [func( x * 0.2) for x in xlist]

pylab.plot(xlist, ylist1)
pylab.plot(xlist, ylist2)

pylab.show()
