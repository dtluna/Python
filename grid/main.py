#!/usr/bin/env python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from datetime import datetime

time_format = '{hour}:{minute}:{second}.{microsecond}'

def insert_time(time):
    return time_format.format(
        hour=time.hour,
        minute=time.minute,
        second=time.second,
        microsecond=time.microsecond
    )

ui_file = 'ui.glade'
RIGHT = 1
LEFT = 0
b = Gtk.Builder()
b.add_from_file(ui_file)
w = b.get_object('window')
w.connect('destroy', Gtk.main_quit)
w.show_all()

positions = {LEFT: Gtk.PositionType.RIGHT, RIGHT: Gtk.PositionType.BOTTOM}


def add_row(self, left_content, right_content):
    """
    Adds row to a Gtk.Grid with two columns.
    First the right row is added, then left. It is done so because when 
    there is nothing in the Gtk.Grid its 'attach_next_to()' method inserts new
    child at the bottom left of Grid. So first we insert a child to left bottom,
    and then we insert a child to the left of existing child.
    """
    def add_part_row(side, content):
        self.attach_next_to(
            child=content,
            sibling=self.get_child_at(left=side, top=self.last_row),
            side=positions[side],
            width=1,
            height=1
        )
    add_part_row(RIGHT, right_content)
    print(self.get_children())
    add_part_row(LEFT, left_content)
    print(self.get_children())
    
    self.last_row += 1
    self.show_all()

def label_to_str(self):
    return self.get_text()


setattr(Gtk.Grid, 'add_row', add_row)
setattr(Gtk.Grid, 'last_row', 0)
setattr(Gtk.Label, '__str__', label_to_str)
setattr(Gtk.Label, '__repr__', label_to_str)

g = b.get_object('grid')

def a(widget=None, data=None):
    left_time = datetime.now()
    right_time = datetime.now()
    left = Gtk.Label(insert_time(left_time))
    right = Gtk.Label(insert_time(right_time))
    g.add_row(left, right)


b.connect_signals({'add_row': a})

if __name__ == '__main__':
    Gtk.main()
