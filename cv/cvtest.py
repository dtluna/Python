import cv2
from time import sleep

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2HLS_FULL)

    # Display the resulting frame
    cv2.imshow('frame',gray)
    cv2.imwrite("filename.jpg",frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
sleep(1)
