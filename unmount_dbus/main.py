#!/usr/bin/env python3

def unmount(button, partition):
    bus = dbus.SystemBus()
    proxy = bus.get_object('org.freedesktop.UDisks',
                           '/org/freedesktop/UDisks')
    iface = dbus.Interface(proxy, 'org.freedesktop.UDisks')
    for dev in iface.EnumerateDevices():
        dev_obj = bus.get_object('org.freedesktop.UDisks', dev)
        dev_prop = dbus.Interface(dev_obj, 'org.freedesktop.DBus.Properties')
        if dev_prop.Get('', 'DeviceFile') == partition:
            idev = dbus.Interface(
                dev_obj, 'org.freedesktop.DBus.UDisks.Device')
            try:
                idev.get_dbus_method(
                    'FilesystemUnmount',
                    dbus_interface='org.freedesktop.UDisks.Device'
                )([])
            except dbus.exceptions.DBusException as e:
                dialog = Gtk.MessageDialog(
                    message_type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.CLOSE,
                    title="DBus error",
                    text=e.get_dbus_message()
                )
                dialog.run()
                dialog.destroy()

if __name__ == '__main__':
    pass