#!/usr/bin/env python3

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
from os.path import exists
import rlcompleter
import readline
import atexit
readline.parse_and_bind("tab: complete")
Gst.init(None)

HISTORY_FILE = '.history'
if exists(HISTORY_FILE):
    readline.read_history_file(HISTORY_FILE)
else:
    open(HISTORY_FILE, 'w')

atexit.register(readline.write_history_file, HISTORY_FILE)
playbin = Gst.ElementFactory.make('playbin', None)
pulsesink = Gst.ElementFactory.make('pulsesink', 'pulsesink')
playbin.set_property('audio-sink', pulsesink)


def play(filepath=None, *args):
    if filepath is None:
        try:
            filepath = input('Enter absolute file path:')
        except (EOFError, KeyboardInterrupt):
            print()
            return
    if exists(filepath):
        playbin.set_property('uri', 'file://' + filepath)
        playbin.set_state(Gst.State.PLAYING)
    else:
        print('No such file: \'{}\''.format(filepath))


def pause(*args):
    playbin.set_state(Gst.State.PAUSED)


def resume(*args):
    playbin.set_state(Gst.State.PLAYING)


def print_help(*args):
    for cmd in commands:
        print(cmd)


commands = {'play': play, 'pause': pause, 'resume': resume, 'help': print_help, 'quit': quit}

if __name__ == '__main__':
    while True:
        try:
            user_input = input('>>>')
        except (EOFError, KeyboardInterrupt):
            quit()
        if user_input:
            command, *arguments = user_input.split(maxsplit=1)
            if command in commands:
                commands[command](*arguments)
            else:
                print('No such command')
