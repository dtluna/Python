#!/usr/bin/env python3

import sys
import time

if __name__ == '__main__':
    for i in range(100):
        sys.stdout.write("Downloading ... %s%%\r" % (i))
        time.sleep(0.1)
