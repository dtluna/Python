#!/usr/bin/env python3

from subprocess import call
from glob import glob
from os import chdir, path
from argparse import ArgumentParser

parser = ArgumentParser(
    description="Unrar all .rar archives in given directory.")
parser.add_argument('dir', type=str,
                    help='directory to do unrar in',
                    metavar='dir')

if __name__ == '__main__':
    directory = parser.parse_args().dir
    if path.exists(path.abspath(directory)):
        rar_names = glob('*.rar')
    try:
        for rar in rar_names:
            call(['unrar', 'x', rar])
    except (EOFError, KeyboardInterrupt):
            exit()
