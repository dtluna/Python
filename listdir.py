#!/usr/bin/env python3
from os import listdir, chdir
from os.path import isdir, abspath
from subprocess import call
from glob import glob


def get_sources():
    return glob('*.c')


def dirs():
    return sorted(list(filter(isdir, listdir())))


def indent(sources):
    if sources:
        for s in sources:
            print('indent {}'.format(s))
            res = call('indent {}'.format(s), shell=True)
            if res != 0:
                raise RuntimeError(abspath('.'))


def enter_dir(dir):
    chdir(dir)
    print('Entering {dir}'.format(dir=abspath('.')))
    for d in dirs():
        enter_dir(d)
    sources = get_sources()
    indent(sources)
    print('Exiting {dir}'.format(dir=abspath('.')))
    chdir('..')


if __name__ == '__main__':
    for d in dirs():
        enter_dir(d)
