#!/usr/bin/env python3

from pony.orm import *


db = Database('sqlite', 'test_db.sqlite', create_db=True)


class Person(db.Entity):
    name = Required(str)
    age = Required(int)
    cars = Set('Car')


class Car(db.Entity):
    make = Required(str)
    model = Required(str)
    owner = Required(Person)


db.generate_mapping(create_tables=False, check_tables=True)


if __name__ == '__main__':
    with db_session:
        ps = select(p for p in Person)
        ps.show()
